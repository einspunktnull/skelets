import {Ask} from '../core/Ask';
import {AskDef} from '../core/AskDef';
import {NpmUtil} from '@beautils/all';

export class AskForPackageName extends Ask<string> {
    protected createDefinition(): AskDef<string> {
        return {
            type: "input",
            message: 'Enter package name',
            default: 'awesome-package',
            validate: async (name: string) => {
                return NpmUtil.validatePackageName(name);
            }
        };
    }
}
