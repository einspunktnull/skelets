import {Ask} from '../core/Ask';
import {AskDef} from '../core/AskDef';
import {PathUtil} from '@beautils/all';

export class AskForPackagePath extends Ask<string> {

    private readonly proposal: string;

    public constructor(defaultValue: string) {
        super();
        this.proposal = defaultValue;
    }

    protected createDefinition(): AskDef<string> {
        return {
            type: "input",
            message: 'Enter package path',
            default: this.proposal,
            validate: async (name: string) => {
                return PathUtil.validate(name);
            }
        };
    }

}

