import {Action} from '../core/Action';
import path from 'path';
import {CopyFilesUtil, FsUtil, NpmUtil, PackageJsonRef, PromiseUtil} from '@beautils/all';

export abstract class BaseAction extends Action {

    public abstract get id(): string ;

    protected async getTemplateDir(): Promise<string> {
        return await this.getSkeletonDir() + path.sep + this.id;
    }

    protected async getSkeletonDir(): Promise<string> {
        return await NpmUtil.getPackagePath(__dirname) + path.sep + 'skeletons';
    }

    protected async copySkeletonTo(packagePath: string): Promise<void> {
        const templateDir: string = await this.getTemplateDir();
        await CopyFilesUtil.copyDirContentsInto(templateDir, packagePath);
    }

    protected async updatePackageJson(packageName: string, packagePath: string, description: string, author: string): Promise<void> {
        const packageJsonTemplatePath: string = packagePath + path.sep + 'package_template.json';
        const packageJsonPath: string = packagePath + path.sep + 'package.json';
        await FsUtil.rename(packageJsonTemplatePath, packageJsonPath);

        const packageJson: PackageJsonRef = await PackageJsonRef.bind(packageJsonPath);

        packageJson
            .setName(packageName)
            .setAuthor(author)
            .setDescription(description);

        const devDeps: Record<string, string> = packageJson.getDevDependencies();
        if (devDeps) {
            const devDepsWithLatest: Record<string, string> = await this.getLatestVersions(devDeps);
            packageJson.setDevDependencies(devDepsWithLatest);
        }

        const deps: Record<string, string> = packageJson.getDependencies();
        if (deps) {
            const depsWithLatest: Record<string, string> = await this.getLatestVersions(deps);
            packageJson.setDependencies(depsWithLatest);
        }

        const optDeps: Record<string, string> = packageJson.getOptionalDependencies();
        if (optDeps) {
            const optDepsWithLatest: Record<string, string> = await this.getLatestVersions(optDeps);
            packageJson.setOptionalDependencies(optDepsWithLatest);
        }

        const peerDeps: Record<string, string> = packageJson.getPeerDependencies();
        if (peerDeps) {
            const peerDepsWithLatest: Record<string, string> = await this.getLatestVersions(peerDeps);
            packageJson.setPeerDependencies(peerDepsWithLatest);
        }

        await packageJson.commit();

    }

    protected async getLatestVersions(dependencies: Record<string, string>): Promise<Record<string, string>> {
        return PromiseUtil.allForEach(dependencies, async (version: string, key: string) => {
            return '^' + await NpmUtil.getLatestPackageVersion(key);
        });
    }

}
