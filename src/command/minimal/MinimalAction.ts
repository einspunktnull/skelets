import {MinimalCommandDef} from './MinimalCommand';
import {AskForPackageName} from '../../ask/AskForPackageName';
import {Ask} from '../../core/Ask';
import path from 'path';
import {AskForPackagePath} from '../../ask/AskForPackagePath';
import {FatalError} from '../../core/FatalError';
import {ExitCode} from '../../core/ExitCode';
import {FsUtil, NpmUtil} from '@beautils/all';
import {BaseAction} from '../BaseAction';

export class MinimalAction extends BaseAction {

    public get id(): string {
        return MinimalCommandDef.name;
    }

    public async execute(): Promise<void> {
        const packageName: string = await this.obtainPackageName();
        const packagePath: string = await this.obtainPackagePath(packageName);
        const packagePathExists: boolean = await FsUtil.exists(packagePath);
        if (packagePathExists) {
            console.warn(`the path ${packagePath} already exists.`);
            const overwriteExisting: boolean = await this.obtainOptionOverwriteExisting(packagePath);
            if (!overwriteExisting) this.errorService.outputErrorAndExit(
                new FatalError('package creation aborted by the user'),
                ExitCode.ABORTED
            );
        }
        await this.copySkeletonTo(packagePath);

        const description: string = await this.obtainDescription(packageName);
        const author: string = await this.obtainAuthor(packageName);

        await this.updatePackageJson(packageName, packagePath, description, author);

        const install: boolean = await this.obtainOptionInstall();
        if (install) {
            await NpmUtil.installSelf(packagePath);
            const runAfterInstall: boolean = await this.obtainOptionRunAfterInstall();
            if (runAfterInstall) await NpmUtil.runScript('start', packagePath);
        }

    }

    private async obtainPackageName(): Promise<string> {
        return await this.getInputValueOrAsk<string>(
            MinimalCommandDef.args.packageName,
            new AskForPackageName(),
        );
    }

    private async obtainDescription(packageName: string): Promise<string> {
        return await this.ask<string>(
            Ask.create<string>({
                type: "input",
                message: `Enter description`,
                default: `Description of ${packageName}`,
            }),
        );
    }

    private async obtainAuthor(packageName: string): Promise<string> {
        return await this.ask<string>(
            Ask.create<string>({
                type: "input",
                message: `Enter author's name`,
                default: `Developer of ${packageName}`,
            }),
        );
    }

    private async obtainPackagePath(packageName: string): Promise<string> {
        const proposal: string = '.' + path.sep + packageName;
        const relOrAbsPath: string = await this.getInputValueOrAsk<string>(
            MinimalCommandDef.args.packagePath,
            new AskForPackagePath(proposal)
        );
        return path.resolve(relOrAbsPath);
    }

    private async obtainOptionOverwriteExisting(path: string): Promise<boolean> {
        return await this.getInputValueOrAsk<boolean>(
            MinimalCommandDef.opts.overwriteExisting,
            Ask.create<boolean>({
                type: "confirm",
                message: `Do you wish to overwrite existing files at ${path}?`,
                default: false,
            }),
        );
    }

    private async obtainOptionInstall(): Promise<boolean> {
        return await this.getInputValueOrAsk<boolean>(
            MinimalCommandDef.opts.install,
            Ask.create<boolean>({
                type: "confirm",
                message: 'Do you wish to install?',
                default: false,
            }),
        );
    }

    private async obtainOptionRunAfterInstall(): Promise<boolean> {
        return await this.getInputValueOrAsk<boolean>(
            MinimalCommandDef.opts.runAfterInstall,
            Ask.create<boolean>({
                type: "confirm",
                message: 'Do you wish to run?',
                default: false,
            }),
        );
    }

}
