import {Command} from '../../core/Command';
import commander from 'commander';

// type Record<K extends keyof any, T> = {
//     [P in K]: T;
// };

export const MinimalCommandDef = {
    name: 'minimal',
    args: {
        packageName: 'package-name',
        packagePath: 'package-path',
    },
    opts: {
        overwriteExisting: 'overwrite-existing',
        install: 'install',
        runAfterInstall: 'run-after-install',
    },
};

export class MinimalCommand extends Command {

    public load(program: commander.CommanderStatic): void {
        program
            .command([
                'minimal',
                `[${MinimalCommandDef.args.packageName}]`,
                `[${MinimalCommandDef.args.packagePath}]`
            ].join(' ')) // ex: skelets minimal my-project ./my-project
            .aliases(['m', 'mini'])
            .description('create a typescript project')
            .option(`-o, --${MinimalCommandDef.opts.overwriteExisting}`, 'enforces overwrite if directory already exists')
            .option(`-i, --${MinimalCommandDef.opts.install}`, 'run npm install after creation')
            .option(`-r, --${MinimalCommandDef.opts.runAfterInstall}`, 'run after npm install')
            .action(async (
                packageName: string,
                packagePath: string,
                command: commander.Command,
            ) => {
                await this.action.handle([
                    {name: MinimalCommandDef.args.packageName, value: packageName},
                    {name: MinimalCommandDef.args.packagePath, value: packagePath},
                    {name: MinimalCommandDef.opts.overwriteExisting, value: command.overwriteExisting},
                    {name: MinimalCommandDef.opts.install, value: command.install},
                    {name: MinimalCommandDef.opts.runAfterInstall, value: command.runAfterInstall},
                ]);
            });
    }

}

