export class FatalError extends Error {

    private readonly _payload: any;

    public constructor(message: string, payload?: any) {
        super(message);
        this._payload = payload;
    }

    get payload(): any {
        return this._payload;
    }
}
