import {CommanderStatic} from 'commander';
import {Action} from './Action';

export abstract class Command {
    public constructor(
        protected action: Action
    ) {}

    public abstract load(program: CommanderStatic): void;
}
