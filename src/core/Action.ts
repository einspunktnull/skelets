import {Ask} from './Ask';
import {isString} from 'lodash';
import {BooleanOrString, Input, InputRawValue, TransformFunction, ValidateFunction} from './types';
import {Inject} from 'typescript-ioc';
import {ErrorService} from './ErrorService';
import {FatalError} from './FatalError';
import {ExitCode} from './ExitCode';

export abstract class Action {

    @Inject
    protected readonly errorService: ErrorService;

    private readonly inputs: Map<string, Input> = new Map<string, Input>();

    public abstract execute(): Promise<void> ;

    public async handle(inputs?: Input[]): Promise<void> {
        inputs?.forEach(i => this.inputs.set(i.name, i));
        try {
            return this.execute();
        } catch (e) {
            throw this.errorService.outputErrorAndExit(
                new FatalError('unknown error', e),
                ExitCode.UNKNOWN_ERROR
            );
        }
    }

    protected async getInputValue<T>(
        key: string,
        validate?: ValidateFunction,
        transform?: TransformFunction<T>,
    ): Promise<T> {
        const rawValue: InputRawValue = this.inputs.get(key)?.value;
        if (rawValue === undefined) return undefined;
        if (validate) {
            const validationResult: BooleanOrString = await validate(rawValue);
            if (isString(validationResult)) this.errorService.outputErrorAndExit(
                new FatalError('error: input validation failed.', validationResult),
                ExitCode.INVALID_INPUT_VALUE
            );
        }
        if (transform) return await transform(rawValue);
        return <T><unknown>rawValue;
    }

    protected async getInputValueOrAsk<T>(
        key: string,
        ask: Ask<T>,
        validate?: ValidateFunction,
        transform?: TransformFunction<T>
    ): Promise<T> {
        const inputValue: T = await this.getInputValue(
            key,
            validate || ask.definition.validate,
            transform || ask.definition.transform
        );
        if (inputValue !== undefined) return inputValue;
        return this.ask(ask);
    }

    protected async ask<T>(ask: Ask<T>): Promise<T> {
        return ask.execute();
    }


}
