import inquirer, {QuestionCollection} from 'inquirer';
import {DieAndwoord, InputRawValue} from './types';
import {AskDef} from './AskDef';

export abstract class Ask<T> {
    private _def: AskDef<T>;

    public static create<T>(def: AskDef<T>): Ask<T> {
        return new class Uwe extends Ask<T> {
            public createDefinition(): AskDef<T> { return def; }
        };
    }

    public get definition(): AskDef<T> {
        return this._def || (this._def = this.createDefinition());
    }

    protected abstract createDefinition(): AskDef<T>;

    public async execute(): Promise<T> {
        const questionCollection: QuestionCollection<DieAndwoord> = {
            name: 'value',
            type: this.definition.type,
            message: this.definition.message,
            default: this.definition.default,
            validate: this.definition.validate,
        };
        const awnser: DieAndwoord = await inquirer.prompt(questionCollection);
        const rawValue: InputRawValue = awnser.value;
        if (this.definition.transform) return <T><unknown>(await this.definition.transform(rawValue));
        return <T><unknown>rawValue;
    }

}
