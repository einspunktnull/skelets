export type BooleanOrString = boolean | string;
export type InputRawValue = BooleanOrString;

export type DieAndwoord = { value: InputRawValue };

export type ValidateFunction = (rawValue: InputRawValue) => Promise<BooleanOrString>;
export type TransformFunction<T> = (rawValue: InputRawValue) => Promise<T>;

export interface Input {
    name: string;
    value: InputRawValue;
    options?: any;
}
