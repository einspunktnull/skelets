import commander, {CommanderStatic} from 'commander';
import {Command} from './Command';
import {ErrorService} from './ErrorService';
import {Inject} from 'typescript-ioc';
import {ExitCode} from './ExitCode';
import {FatalError} from './FatalError';

export class Application {

    @Inject
    private readonly errorService: ErrorService;

    private readonly program: CommanderStatic = commander;
    private readonly version: string;
    private readonly commands: Command[];

    public constructor(version: string, commands: Command[]) {
        this.version = version;
        this.commands = commands;
    }

    public run(processArgv: string[]): void {
        this.program
            .version(
                this.version,
                '-v, --version',
                'output the current version'
            )
            .usage('<command> [options]')
            .helpOption('-h, --help', 'Output usage information.');

        this.commands.forEach(c => c.load(this.program));

        this.handleInvalidCommand();

        this.program.parse(processArgv);

        if (!process.argv.slice(2).length) {
            this.program.outputHelp();
        }
    }

    protected handleInvalidCommand() {
        this.program.on('command:*', () => {
            this.errorService.outputErrorAndExit(new FatalError(
                'Invalid command ' + this.program.args.join(' '),
                `See --help' for a list of available commands.`
            ), ExitCode.INVALID_COMMANMD);
        });
    }
}
