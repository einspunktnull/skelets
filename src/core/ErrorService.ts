import {OnlyInstantiableByContainer, Singleton} from 'typescript-ioc';
import {Subject, Subscription} from 'rxjs';
import {FatalError} from './FatalError';

@Singleton
@OnlyInstantiableByContainer
export class ErrorService {

    private readonly errorSubject: Subject<Error> = new Subject<Error>();

    public outputErrorAndExit(error: FatalError, exitCode: number): FatalError {
        this.errorSubject.next(error);
        console.error(error.message);
        if (error.payload) console.log(error.payload);
        process.exit(exitCode);
        return error;
    }

    public subscribe(callback: (error: Error) => {}): Subscription {
        return this.errorSubject.subscribe(callback);
    }

}
