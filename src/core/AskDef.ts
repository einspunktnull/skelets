import {QuestionTypeName} from 'inquirer';
import {TransformFunction, ValidateFunction} from './types';

export interface AskDef<T> {
    type: QuestionTypeName;
    message: string;
    default?: T;
    validate?: ValidateFunction,
    transform?: TransformFunction<T>,
}
