export enum ExitCode {
    SUCCESS = 0,
    INVALID_COMMANMD = 1,
    INVALID_INPUT_VALUE,
    ABORTED,
    UNKNOWN_ERROR
}
