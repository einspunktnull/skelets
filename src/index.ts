#!/usr/bin/env node

import 'source-map-support/register';
import fs from 'fs';
import path from 'path';
import {Application} from './core/Application';
import {MinimalAction} from './command/minimal/MinimalAction';
import {MinimalCommand} from './command/minimal/MinimalCommand';

const packageJsonPath: string = path.resolve(`${__dirname}${path.sep}..${path.sep}package.json`);
const packageJsonContent: string = fs.readFileSync(packageJsonPath).toString();
const packageJson: any = JSON.parse(packageJsonContent);

new Application(
    packageJson.version,
    [
        new MinimalCommand(new MinimalAction())
    ]
).run(process.argv);
